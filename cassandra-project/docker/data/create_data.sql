CREATE KEYSPACE dictionary WITH replication = {'class':'SimpleStrategy', 'replication_factor' : 3};
use dictionary;
CREATE TABLE words_meanings (id int PRIMARY KEY,Word text,Meaning text);
COPY words_meanings (id, Word, Meaning) FROM '/data/dictionary.csv' WITH HEADER = FALSE ;
