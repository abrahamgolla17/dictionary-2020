package cassandra.cassandra_project;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * Unit test for simple App.
 */
public class AppTest 
    
{
	
	private static Session session;
	
	public static void main(final String[] args)
	{
		CassandraConnector client = null;
	 try {
		client = new CassandraConnector();
	   final String ipAddress = args.length > 0 ? args[0] : "0.0.0.0";
	   final int port = args.length > 1 ? Integer.parseInt(args[1]) : 9042;
	   System.out.println("Connecting to IP Address " + ipAddress + ":" + port + "...");
	   session = client.connect(ipAddress, port);
	   
	   ResultSet rs = session.execute("SELECT * from dictionary.words_Meanings where word='SATYRIASIS' ALLOW FILTERING");
	   for (Row row : rs) {
		   System.out.println(row);
	   }
	 }
	 finally {
	   client.close();
	 }
	}
}
