package com.kaab.redis.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.kaab.redis.model.Word;

import java.util.Map;
import javax.annotation.PostConstruct;

@Repository
public class RedisRepositoryImpl implements RedisRepository {
    private static final String KEY = "Word";
    
    private RedisTemplate<String, Object> redisTemplate;
    private HashOperations hashOperations;
    
    @Autowired
    public RedisRepositoryImpl(RedisTemplate<String, Object> redisTemplate){
        this.redisTemplate = redisTemplate;
    }

    @PostConstruct
    private void init(){
        hashOperations = redisTemplate.opsForHash();
        Word word = new Word("karthik", "master");
        hashOperations.put(KEY, word.getWord(), word.getMeaning());

    }
    
    public void add(final Word word) {
        hashOperations.put(KEY, word.getWord(), word.getMeaning());
    }

    public void delete(final String word) {
        hashOperations.delete(KEY, word);
    }
    
    public String findWord(final String word){
        return (String) hashOperations.get(KEY, word);
    }
    
    public Map<Object, Object> findAllWords(){
        return hashOperations.entries(KEY);
    }

	
	

  
}