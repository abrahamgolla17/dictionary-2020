package com.kaab.redis.repository;


import java.util.Map;
import java.util.Set;

import com.kaab.redis.model.Word;


public interface RedisRepository {

 
    Map<Object, Object> findAllWords();

    /**
     * Add key-value pair to Redis.
     */
    void add(Word word);

    /**
     * Delete a key-value pair in Redis.
     */
    void delete(String id);
    
    /**
     * find menaing
     */
    String findWord(String id);
    
}