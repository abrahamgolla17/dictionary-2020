package com.kaab.redis.resource;


import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kaab.redis.model.Word;
import com.kaab.redis.repository.RedisRepository;


@RestController
@RequestMapping("/rest/redis")
public class RedisResource {

	@Autowired
	private RedisRepository redisRepository;

	
	@GetMapping(path = "/get/{word}")
	public String getWord(@PathVariable("word") String word) {
		return redisRepository.findWord(word);
	}


	@RequestMapping("/keys")
	public @ResponseBody Map<Object, Object> keys() {
		return redisRepository.findAllWords();
	}

	@RequestMapping("/values")
	public @ResponseBody Map<String, String> findAll() {
		Map<Object, Object> aa = redisRepository.findAllWords();
		Map<String, String> map = new HashMap<String, String>();
		for(Map.Entry<Object, Object> entry : aa.entrySet()){
			String key = (String) entry.getKey();
			map.put(key, aa.get(key).toString());
		}
		return map;
	}

	@PostMapping("/add")
	public ResponseEntity<String> add(@RequestBody Word word) {
		redisRepository.add(word);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public ResponseEntity<String> delete(@RequestParam String key) {
		redisRepository.delete(key);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
