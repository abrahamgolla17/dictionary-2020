package com.kaab.redis.model;

import java.io.Serializable;

public class Word implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String word;
	private String meaning;
	public Word(String name, String meaning){
		this.setWord(name);
		this.meaning = meaning;

	}

	@Override
	public String toString(){
		return "Word{" + " name =" + getWord() + ", meaning =" + meaning + "}" ;
	}
	public String getMeaning() {
		return meaning;
	}
	public void setMeaning(String meaning) {
		this.meaning = meaning;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}
}
