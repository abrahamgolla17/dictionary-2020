package com.kaab.dictionary.facade;

import java.util.List;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.kaab.dictionary.model.Word;
import com.kaab.dictionary.model.WordRedis;
import com.kaab.dictionary.resource.controller.DicstionaryRepository;
import com.netflix.appinfo.InstanceInfo;

@Component
public class DictUtil {

	@Autowired
	DicstionaryRepository dictionRepo;

	@Autowired
	private RestTemplate restTemplate;

	public String getMeaningOfWord(String word) {

		String meaning = null ;
		try{
			System.out.println("searching in redis for word "+word);
			meaning = restTemplate.getForObject("http://EUREKA-REDIS-SERVICE/rest/redis/get/"+word, String.class);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(StringUtils.isBlank(meaning)) {
			System.out.println("searching in cassandra as its not in redis");
			
			List<Word> listOfWords = dictionRepo.findByWord(word !=null? word.toUpperCase() : "");
			if(Objects.nonNull(listOfWords) && listOfWords.size() >=1) {
				meaning = listOfWords.get(0).getMeaning();
				insertIntoRedis(word, meaning);
			}else {
				meaning = "Word Not found";
			}

		}else {
			System.out.println("found in redis");
		}

		return meaning;
		
	}

	private void insertIntoRedis(String word, String meaning) {
		HttpEntity<WordRedis> request = new HttpEntity<>(new WordRedis(word, meaning));

		ResponseEntity<WordRedis> body = restTemplate.postForEntity("http://EUREKA-REDIS-SERVICE/rest/redis/add", request, WordRedis.class);
		System.out.println("inserted into redis now \n  "+ body );
	}



}
