package com.kaab.dictionary.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.kaab.dictionary.facade.DictUtil;
import com.kaab.dictionary.model.Word;
import com.kaab.dictionary.model.WordRedis;
import com.kaab.dictionary.resource.controller.DicstionaryRepository;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;


@RestController
@RequestMapping("/word")
public class DictionaryResource {

	@Autowired
	DicstionaryRepository dictionRepo;

	@Autowired
	private EurekaClient discoveryClient;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired 
	private DictUtil dictUtil;
	
	@GetMapping(path = "/{word}")
	public WordRedis getWords(@PathVariable("word") String word) {
	
	//	return dictionRepo.findByWord(word !=null? word.toUpperCase() : "");
		return new WordRedis(word, dictUtil.getMeaningOfWord(word));
	}
	

	public String serviceUrl() {
		
		InstanceInfo instance = discoveryClient.getNextServerFromEureka("EUREKA-REDIS-SERVICE",false);
	    System.out.println(instance);
	    
		return instance.getHomePageUrl();
	}
	
}
