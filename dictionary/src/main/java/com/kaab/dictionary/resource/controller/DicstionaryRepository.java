package com.kaab.dictionary.resource.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;

import com.kaab.dictionary.model.Word;


public interface DicstionaryRepository extends CassandraRepository<Word, UUID> {
  @AllowFiltering
  List<Word> findByWord(String word);   
}