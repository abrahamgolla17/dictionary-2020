package com.kaab.eureka.kaab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class KaabApplication {

	public static void main(String[] args) {
		SpringApplication.run(KaabApplication.class, args);
	}

}
