package redis.redis_project;

import java.util.List;
import java.util.Map;

import redis.clients.jedis.Jedis; 

public class RedisClient {

	private static Jedis jedis;

	public static Jedis initialize(String hostname, int port, boolean ssl) {
		jedis = new Jedis(hostname, port, ssl);
		return jedis;
	}
	

	public static List<String> getHashValue(String hmName, String fields) {
		if(jedis.hexists(hmName, fields)) {
		return jedis.hmget(hmName, fields);
		}
		return null;
	}
	
	public static void setHashValue(String hmName, Map<String, String> map) {
		jedis.hmset(hmName, map);
	}
	
} 
