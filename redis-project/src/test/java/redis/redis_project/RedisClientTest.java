package redis.redis_project;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class RedisClientTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public RedisClientTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( RedisClientTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
    	RedisClient.initialize("localhost", 6379, false);
    	
    	
    	Map<String, String> map = new HashMap();
		map.put("key", "value");

		String fields = "key";

    	RedisClient.setHashValue("test", map);
    	assertEquals("value", RedisClient.getHashValue("test", fields).get(0));
    }
}
