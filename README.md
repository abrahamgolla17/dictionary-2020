# README #

### What is this repository for? ###

* To Learn Microservices

### ARCHITECTURE ###
![Dictionary-Architecture](https://bitbucket.org/abrahamgolla17/dictionary-2020/raw/be33e065f23410912897cb4f8f4d4616fe89abfc/MICROSERVICES%20ARCH.jpg)

### What is the plan? ###

* GET WORD API WITH CACHING
* CASSANDRA DATASTORE
* CLIENT UI

#### Steps
1. First request comes to main controller
2. controller delegates to other service which searches in cache
3. delegates back
4. If not fount delegates request to other service which searches in dictionary 
5. delegates back 
6. respond with search answer and update cache via one more service


#### To run project
1. start cassandra and import data  
    $sh cassandra-project/docker/startup.sh
2. start redis  
    $sh redis-project/docker/startup.sh 
3. start eureka server  
    $cd kaab/  
    $mvn spring-boot:run
4. start redis client  
    $cd kaab-redis/  
    $mvn spring-boot:run
5. start cassandra/dictionary  
    $cd dictionary  
    $mvn spring-boot:run
6. now to test hit http://localhost:8080/word/orange
  
